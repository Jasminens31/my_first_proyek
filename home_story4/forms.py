from django import forms
import datetime 

class mySchedule_form(forms.Form):
	place_attrs = {
		'type': 'text',
		'class': 'todo-form-input',
		'placeholder': 'place'
	}

	place = forms.CharField(label='Tempat', required=True, widget = forms.TextInput(attrs = place_attrs))
	event = forms.CharField(label='Kegiatan',required=True,max_length='25',empty_value='Anonymous')
	category = forms.CharField(label='Kategori', required=True, initial='Event category')
	date = forms.DateField(label='Choose Date', required=True, initial=datetime.date.today, widget=forms.DateInput(attrs={'type' : 'date'}))
	time = forms.TimeField(widget=forms.TimeInput(attrs={'type' : 'time'}))
	
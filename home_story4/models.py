from django.db import models
from django.utils import timezone
from datetime import date, datetime, timedelta
    
class schedule(models.Model):
    place = models.CharField(max_length=20)
    event = models.TextField(max_length=20)
    category = models.CharField(max_length=200)
    date = models.DateField(default=date.today)
    time = models.TimeField(auto_now=False, auto_now_add=False)

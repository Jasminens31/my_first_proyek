from django.contrib import admin
from django.conf import settings
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static

from . import views

app_name = 'jasmine'

urlpatterns = [
	path('', views.welcomepage, name='welcomepage'),
	path('profileku/', views.profile, name='profile'),
	path('regisku/',views.regisku, name="regisku"),
	path('form_fields/', views.form_fields, name='form_fields'),
	path('schedule_post/', views.schedule_post, name='schedule_post'),
	path('deleteJadwal/',views.deleteJadwal, name='deleteJadwal')
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)



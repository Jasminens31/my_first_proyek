from django.shortcuts import render
from .forms import mySchedule_form
from .models import schedule
from django.http import HttpResponseRedirect

# Create your views here.
from django.http import HttpResponse

response = {}

def welcomepage(request):
	return render(request, "home_story3.html")

def profile(request):
	return render(request, "profile_story3.html")

def regisku(request):
	return render(request, "regis_story3.html")

def form_fields(request):
	response['form'] = mySchedule_form
	html = 'displayForm.html'
	return render(request, html, response)

def schedule_post(request):
	form = mySchedule_form(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		response['place'] = request.POST['place'] if request.POST ['place'] != "" else "anonymous"
		response['event'] = request.POST['event'] if request.POST ['event'] != "" else "anonymous"
		response['category'] = request.POST['category'] if request.POST ['category'] != "" else "anonymous"
		response['date'] = request.POST.get('date')
		response['time'] = request.POST['time']

		schedules = schedule(place=response['place'] , event=response['event'], category=response['category'], date=response['date'],
			time=response['time'])

		schedules.save()

		schedules = schedule.objects.all()
		response['schedules'] = schedules
		html = 'table.html'

		return render(request, html, response)


def deleteJadwal(request):
	schedule.objects.all().delete()
	response={}
	html = 'table.html'

	return render(request,html,response)

